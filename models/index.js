const db = [
    {
        id:1,
        personalInfo: {nombre: 'Victor Gabriel', apellido: 'Acencio Vargas', edad: 27, ide: 'intellij'},
        preferedColor: 'azul'
    },
    {
        id:2,
        personalInfo: {nombre: 'Gian Marco', apellido: 'Signago', edad: 45, ide: 'vscode'},
        preferedColor: 'blanco'
    },
    {
        id:3,
        personalInfo: {nombre: 'Carlos', apellido: 'Villagrán', edad: 35, ide: 'atom'},
        preferedColor: 'azul'
    },
    {
        id:4,
        personalInfo: {nombre: 'Marco', apellido: 'Polo', edad: 32, ide: 'vim'},
        preferedColor: 'celeste'
    },
    {   
        id:5,
        personalInfo: {nombre: 'Ricardo', apellido: 'Milos', edad: 33, ide: 'block de notas'},
        preferedColor: 'rojo'
    },
]

module.exports = {db};