//capa de servicio encargada de la implemetación de la lógica de negocio, además de nexo entre el adaptador y controladores

//importamos las funciones de la capa controller
const { IDEController, ColorController, FindUser, ExistUser } = require("../controllers");

const Service = ({id}) => {
  try {
    const existUser = ExistUser({id});
    if(!existUser.data) throw Error(existUser.message);

    const findUser = FindUser({id});
    if(findUser.data.personalInfo.edad < 18) throw Error('Eres menor de edad no puedes continuar');
    return {statusCode: 200,data: findUser.data,message: 'éxito'};
    
  } catch (error) {
    console.log({step:'Service',error: error.message});
    return {statusCode: 500, message: error.message};
  }
  /*let step;
  try {
    if(Number.isNaN(+edad)) throw Error('Qué clase de edad es esa?');
    if (edad >= 18) {
      let dataColorController = ColorController({ color, edad });
      let dataIDEController = IDEController({ ide });
      if (dataColorController.codigo != 200) {
        step = dataColorController.step;
        throw Error(dataColorController.mensaje);
        
      }
      if (dataIDEController.codido != 200) {
        step = dataIDEController.step;
        throw Error(dataIDEController.mensaje);
      }
      return {
        codigo: 200,
        data: {
          nombre,
          apellido,
          edad,
          color: dataColorController.color,
          ide: dataIDEController.ide,
        },
        mensaje: "éxito",
      };
    } else throw Error("Eres menor de edad, no puedes continuar");
  } catch (error) {
    return {
      codigo: 500,
      step: step ?? "Service",
      mensaje: error.message ?? "error",
    };
  }*/
};

module.exports = { Service };
