//simulando el consumo de datos de una db
const {db} = require('../models');

const FindUser= ({id})=>{
    try{
        const user = db.filter(us=>us.id === id)[0];
        return {statusCode: 200, data: user,message:'éxito'}

    }catch(error){
        console.log({step:'Controller/FindUser',error: error.message});
        return {statusCode: 500, message: error.message};
    }
}

const ExistUser = ({id})=> {
    try {
        if(db.some(user => user.id===id)) return {statusCode: 200, data: true, message: 'éxito'};
        return {statusCode: 400, data: false , message: 'No tenemos registrado a algún usuario con dicho id'};

    } catch (error) {
        console.log({step: 'Controller/ExistUser', error: error.message});
        return {statusCode: 500, message: error.message};
    }
}
/*
const IDEController = ({ide})=>{
    try{
        if(ide === 'intellijIdea') console.log('Este ide pertenece a Jetbrains');
        else if(ide === 'vim') console.log('Este ide le pertenece al publico');
        else if(ide ==='vscode') console.log('Este ide le pertenece a microsoft');
        
        return ({codido: 200, ide, mensaje : 'éxito'});
    } catch(error){
        return ({codigo: 500, step: 'IDEController', mensaje: error.message ?? 'error'});
    }
}

const ColorController= ({color,edad})=>{
    try {
        if(edad>= 18){
            if (color ==='azul') color= 'rojo';
            else if (color === 'rojo') color =  'azul';
            else if (color === 'blanco') color = 'negro';
            else if (color === 'negro') color = 'blanco';
            else color='color por defecto';
        }

        return {codigo: 200, color, mensaje:'éxito' }
    } catch (error) {
        console.log()
        return {codigo: 500, step: 'ColorController', mensaje: error.message ?? 'error'};
    }
}
*/

module.exports = {/*IDEController, ColorController,*/FindUser,ExistUser};