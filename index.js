//simulación de un agente externo que usa nuestro microservicio

//importamos Adapter
const {Adapter} = require("./src/Adapter");
const main = async ({id})=>{

    try {
        const {statusCode,data,message}= await Adapter({id});
        return {statusCode,data,message};
    } catch (error) {
        console.log({step: 'Adapter', error: error.toString()})
        return {statusCode: 500, message: error.message};
    }

    /*let step;
    try{
    const dataAdapter = Adapter({nombre,apellido,edad,color,ide});
        if(dataAdapter.codigo != 200) {step = dataAdapter.step;throw Error(dataAdapter.mensaje);}
    
    return {codigo: 200, data: dataAdapter.data, mensaje: 'éxito'};
    }
    catch(error){
        
        console.log(error.toString());
        return {codigo: 500, step: step?? 'conexion', mensaje:error.toString() ?? 'error'};
    }*/
}
const result = async ()=>console.log(await main({id:4}));
result();