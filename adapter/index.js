const { Service } = require("../service");

const Adapter = ({ id }) => {

  try{
    const {statusCode,data,message}= Service({id});
    return {statusCode,data,message};

  }catch(error){
    console.log({step: 'Adapter', error: error.toString()})
    return {statusCode: 500, message: error.message};
  }
  /*let step;
  try {
    const dataService = Service({ nombre, apellido, edad, color, ide });
    if (dataService.codigo !== 200) {
      step = dataService.step;
      throw Error(dataService.mensaje);
    }
    return { codigo: 200, data: dataService.data, mensaje: "éxito" };
  } catch (error) {
    return {
      codigo: 500,
      step: step ?? "Adapter",
      mensaje: error.message ?? "Error",
    };
  }*/
};

module.exports = { Adapter };
